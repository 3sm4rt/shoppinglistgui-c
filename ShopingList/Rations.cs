﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopingList
{
    class Rations
    {
        private string name;
        private double packagin;
        private string type;
        private double cost;

        public Rations(string name, double packagin, string type, double cost)
        {
            this.name = name;
            this.packagin = packagin;
            this.type = type;
            this.cost = cost;
        }
        public string Name
        {
            get
            {
                return name;
            }
        }
        public double Packagin
        {
            get
            {
                return packagin;
            }
        }
        public string Type
        {
            get
            {
                return type;
            }
        }
        public double Cost
        {
            get
            {
                return cost;
            }
        }

        public override string ToString()
        {
            string result = name + packagin + type  + cost;
            return result;
        }
    }
}
