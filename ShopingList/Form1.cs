﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ShopingList
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text) || string.IsNullOrEmpty(txtPack.Text) || string.IsNullOrEmpty(txtType.Text) || string.IsNullOrEmpty(txtCost.Text))
                return;

            Rations prod = new Rations(txtName.Text, double.Parse(txtPack.Text), txtType.Text, double.Parse(txtCost.Text));

            if (Unit.Text.Equals("Liter") || Unit.Text.Equals("Kg") || Unit.Text.Equals("Darab"))
            {
                
                

                string name = prod.Name;
                double pack = prod.Packagin;
                string type = prod.Type;
                double cos = prod.Cost;

                double gt = pack * cos;

                ListViewItem item = new ListViewItem(name);
               
                item.SubItems.Add(pack + " " + Unit.Text);
                item.SubItems.Add(type);
                item.SubItems.Add(gt + " " + " Ft");
                item.SubItems.Add(Collective.Text);
                listView.Items.Add(item);
                txtName.Clear();
                txtPack.Clear();
                txtType.Clear();
                txtCost.Clear();

            }
        }
            

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView.Items.Count > 0)
                listView.Items.Remove(listView.SelectedItems[0]);

            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Nincs kijelölt elem");      
            }
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            try
            {
                using StreamWriter iras = new StreamWriter(@"N:\Munka\ShoppingList", false, Encoding.UTF8);
                if (listView.Items.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Name Packagin Type CostCollective Name");
                    foreach(ListViewItem item in listView.Items)
                    {
                        sb.AppendLine(string.Format("{0},{1},{2},{3},{4}", item.SubItems[0].Text, item.SubItems[1].Text, item.SubItems[2].Text, item.SubItems[3].Text, item.SubItems[4].Text));
                    }
                    await iras.WriteLineAsync(sb.ToString());
                    MessageBox.Show("Az írás sikeresen megtörtént","Message",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
                    


            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("A könyvtár nem létezik!");
            }
            catch (IOException)
            {
                MessageBox.Show("Hiba a fájl írása közben");
            }
        }
    }
}
